import React from 'react';
import { Navbar } from './commonfiles/Navbar';
import './App.css';
import { Home } from './components/Home';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Ramuygulamari } from './components/Ramuygulamari';
import { Ramuygulamari1 } from './commonfiles/Ramuygulamari1';
import { Products } from './components/Products';
import { SignUp } from './components/SignUp';
import { Unites } from './components/Unites.js';
import { Media } from './components/Media.js';
import { Contact } from './components/Contact';
import { Corporate} from './components/Corporate';

export const App = () =>{
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/corporate' component={Corporate} />
          <Route path='/ramuygulamari' component={Ramuygulamari} />
          <Route path='/ramuygulamari1' component={Ramuygulamari1} />
          <Route path='/products' component={Products} />
          <Route path='/sign-up' component={SignUp} />
          <Route path='/unites' component={Ramuygulamari} />
          <Route path='/media' component={Media} />
          <Route path='/contact' component={Contact} />
        </Switch>
      </Router>
    </>
  );
}

