import React from 'react';
import '../App.css';
import { Cards } from '../commonfiles/Cards';
import HeroSection from '../commonfiles/HeroSection';
import Footer from '../commonfiles/Footer';
import Threed from '../commonfiles/Threed';

export const Home = () => {
  return (
    <>
      {/* <Threed /> */}
      <HeroSection />
      <Cards />
      <Footer />
    </>
  );
}

